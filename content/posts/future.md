---
title: "Future"
date: 2018-05-07T23:24:24+03:00
draft: true
---

### Service workers

+ https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
+ https://www.youtube.com/watch?v=SmZ9XcTpMS4
+ https://jakearchibald.com/2014/using-serviceworker-today/
+ https://developers.google.com/web/fundamentals/getting-started/push-notifications/step-03?hl=en
+ https://developers.google.com/web/fundamentals/getting-started/push-notifications/?hl=en

+ https://kloudsec.com/
+ https://una.im/CSSgram/

### Images

+ https://imageoptim.com/
+ https://pngmini.com/
+ https://github.com/svg/svgo
+ https://developers.google.com/speed/webp/

### PerfTools

+ https://developer.mozilla.org/en/docs/Web/CSS/will-change
+ https://developer.mozilla.org/en/docs/Web/HTML/Element/picture
+ https://github.com/scottjehl/picturefill
+ http://cssstats.com/
+ http://cssdig.com/
+ https://imageoptim.com/

### Css Vars

+ https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables
+ https://www.youtube.com/watch?v=4IRPxCMAIfA
+ https://developers.google.com/web/updates/2016/02/css-variables-why-should-you-care

+ https://codepen.io/collection/DNzQqY/
+ https://madebymike.com.au/writing/using-css-variables/