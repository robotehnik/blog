---
title: "About me"
layout: "about"
resources:
- src: images/me.jpg
  title: "Anton Alekseev is on vacantion"
---
Hi, my name is Anton Alekseev and I am a passionate developer, ready for a new challenge.

My current field of expertise is primary Python 2 / 3 and web-development, but I'd like to get my feets wet in GO or TypeScript. I am also a gamer, artist and a decent cook.