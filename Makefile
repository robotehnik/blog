BASEDIR=$(CURDIR)
OUTPUTDIR=$(BASEDIR)/public

help:
	@echo 'Makefile for a Hugo generated site                                        '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make html                           (re)generate the web site          '
	@echo '   make clean                          remove the generated files         '
	@echo '   make publish                        upload site via SSH                '
	@echo '   make serve [PORT=1313]              serve site at http://localhost:1313'
	@echo '                                                                          '

html:
	hugo

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

serve:
ifdef PORT
	hugo server -D -p $(PORT)
else
	hugo server -D
endif

compress:
	yuicompressor themes/blackbart/static/css/styles.css --type css -o themes/blackbart/layouts/partials/styles.css

deploy:
	sudo scp -P $(SSH_PORT) -r $(OUTPUTDIR)/* $(SSH_USER)@$(SSH_HOST):$(SSH_DIR)

publish: html deploy clean

.PHONY: html help clean serve compress deploy publish
